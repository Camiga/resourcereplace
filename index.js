// Import JIMP for image resizing, and fs for text writing and directory creation.
const fs = require('fs');
const Jimp = require('jimp');

// https://aka.ms/resourcepacktemplate
const originalDir = 'vanilla';
// Output of this script.
const modifiedDir = 'modified';

// Read language file, remove comments (lines beginning with ##), and replace everything after "=" with a string.
const langFile = fs.readFileSync(`${originalDir}/texts/en_US.lang`, {
  encoding: 'utf8',
});
const clearLang = langFile.replace(/##(.*)/g, '');
const repLang = clearLang.replace(/=(.*)/g, '=SAMPLE TEXT');

// Create texts directory and write modified language file.
fs.mkdirSync(`${modifiedDir}/texts/`, { recursive: true });
fs.writeFileSync(`${modifiedDir}/texts/en_US.lang`, repLang);

// Recurring function to search originalDir of writeable images.
async function searchDirectory(dir) {
  await console.log('Searching ' + dir);

  // Read each item in current directory as async array.forEach
  await fs.readdirSync(dir).forEach(async (item) => {
    const fullPath = `${dir}/${item}`;

    // If item is not a file, re-run searchDirectory() on it.
    if (await fs.lstatSync(fullPath).isDirectory())
      await searchDirectory(fullPath);
    else {
      let originalWidth = 0;
      let originalHeight = 0;

      if (item.match(/\.png|\.jpg/g)) {
        await console.log('Reading PNG/JPG ' + fullPath);
        const originalImage = await Jimp.read(fullPath);
        originalWidth = originalImage.bitmap.width;
        originalHeight = originalImage.bitmap.height;
      }

      // JIMP can not read .tga files, so manually get width and height from headers in buffer.
      if (item.match(/\.tga/g)) {
        await console.log('Reading TGA ' + fullPath);
        const originalTGA = await fs.readFileSync(fullPath);
        originalWidth = originalTGA.readInt16LE(12);
        originalHeight = originalTGA.readInt16LE(14);
      }

      // Width and height will be non-zero if file is a supported image.
      if (originalWidth) {
        // Move to modifiedDir for writing new image with JIMP.
        const newPath = await fullPath.replace(originalDir, modifiedDir);

        // Place image in root directory. image.png is stretched to dimensions of currently read file, and written to modifiedDir.
        const newImage = await Jimp.read('image.png');
        await console.log(newPath, originalWidth, originalHeight);

        // NOTE: JIMP will write .tga files as PNG's in their original path.
        return newImage.resize(originalWidth, originalHeight).write(newPath);
      }
    }
  });
}

searchDirectory(originalDir);
